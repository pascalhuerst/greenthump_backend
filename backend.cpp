#include <iostream>

#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/builder/basic/document.hpp>
#include <bsoncxx/builder/basic/array.hpp>
#include <bsoncxx/builder/basic/helpers.hpp>
#include <bsoncxx/json.hpp>

#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>

#include <atomic>
#include <chrono>
#include <thread>
#include <map>

#include <fstream>

#include "json.hpp"

using json = nlohmann::json;

std::atomic<bool> terminateRequest;

enum DataType {
    Temperature,
    Humidity,
    Level
};

const std::string DataTypeNames[] = {
    "Temperature",
    "Humidity",
    "Level"
};

const std::string encodeDateTime(time_t t)
{
    struct tm *tstruct;
    char buf[80];

    tstruct = std::localtime(&t);
    strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", tstruct);

    return buf;
}

time_t makeTimestamp()
{
    return std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
}

const std::string encodeDateTime()
{
    return encodeDateTime(makeTimestamp());
}

const time_t decodeDateTime(const std::string &s)
{
    struct tm tstruct;

    memset(&tstruct, 0, sizeof(struct tm));
    strptime(s.c_str(), "%Y-%m-%d %H:%M:%S", &tstruct);

    return mktime(&tstruct);
}

int main(int, char**)
{

    auto ts = makeTimestamp();
    auto dt = encodeDateTime(ts);
    auto ts2 = decodeDateTime(dt);


    std::cout << ts << std::endl;
    std::cout << dt << std::endl;
    std::cout << ts2 << std::endl;





    return 0;

    auto dataLogger = [](){

        std::string sensor = "/sys/devices/platform/coretemp.0/hwmon/hwmon4/temp1_input";
        std::ifstream file;
        file.open(sensor);
        std::string temp;

        std::ifstream sensor_json_template("sensor_schema.json");
        json j2;
        sensor_json_template >> j2;

        mongocxx::instance inst{};

        mongocxx::client conn{mongocxx::uri{}};
        auto collection = conn["testdb"]["jsontest"];

        //auto cursor = collection.find({});


        /*
        for (auto&& doc : cursor) {
            //std::cout << bsoncxx::to_json(doc) << std::endl;

            json j3 = json::parse(bsoncxx::to_json(doc));

            std::cout << j3.dump(2) << std::endl;

        }
        */

        int counter = 0;
        std::map<std::string, int> minuteData;

        j2["timestamp"] = makeTimestamp();

        while(!terminateRequest.load()) {

            file.seekg(std::ios_base::beg);
            file >> temp;

            minuteData.insert(std::pair<std::string, int>(std::to_string(counter++), std::stoi(temp)));

            // Every 60 seconds
            if (counter == 60) {

                j2["value_count"] = counter;
                j2["type"] = Temperature;
                j2["values"] = json(minuteData);

                auto document = bsoncxx::from_json(j2.dump());

                collection.insert_one(document.view());

                j2["timestamp"] = makeTimestamp();

                counter = 0;
                minuteData.clear();

                std::cout << "Write: " << encodeDateTime() << std::endl;
            }

            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
    };

    std::thread *t = new std::thread(dataLogger);

    while(1);

    return 0;
}


